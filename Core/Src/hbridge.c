/*
 * hbridge.c
 *
 *  Created on: Apr 3, 2021
 *      Author: arnoutzwartbol
 */
#include "main.h"
#include "cmsis_os.h"
#include "hbridge.h"

extern TIM_HandleTypeDef htim8;

void motor_1_init(void){
  HAL_TIM_PWM_Start(&htim8, TIM_CHANNEL_1);
}

void motor_2_init(void){
  HAL_TIM_PWM_Start(&htim8, TIM_CHANNEL_2);
}

void motor_3_init(void){
  HAL_TIM_PWM_Start(&htim8, TIM_CHANNEL_3);
}

void motor_4_init(void){
  HAL_TIM_PWM_Start(&htim8, TIM_CHANNEL_4);
}

void motor_1_setmode(uint8_t mode){
  if (mode==1){
    HAL_GPIO_WritePin(BIN1_MD_GPIO_Port,BIN1_MD_Pin,1);
    HAL_GPIO_WritePin(BIN2_MD_GPIO_Port,BIN2_MD_Pin,0);
  }
  else if (mode==2)
  {
    HAL_GPIO_WritePin(BIN1_MD_GPIO_Port,BIN1_MD_Pin,0);
    HAL_GPIO_WritePin(BIN2_MD_GPIO_Port,BIN2_MD_Pin,1);
  }
  else{
    HAL_GPIO_WritePin(BIN1_MD_GPIO_Port,BIN1_MD_Pin,0);
    HAL_GPIO_WritePin(BIN2_MD_GPIO_Port,BIN2_MD_Pin,0);
  }
}

void motor_4_setmode(uint8_t mode){
  if (mode==1){
    HAL_GPIO_WritePin(BIN1_MB_GPIO_Port,BIN1_MB_Pin,1);
    HAL_GPIO_WritePin(BIN2_MB_GPIO_Port,BIN2_MB_Pin,0);
  }
  else if (mode==2)
  {
    HAL_GPIO_WritePin(BIN1_MB_GPIO_Port,BIN1_MB_Pin,0);
    HAL_GPIO_WritePin(BIN2_MB_GPIO_Port,BIN2_MB_Pin,1);
  }
  else{
    HAL_GPIO_WritePin(BIN1_MB_GPIO_Port,BIN1_MB_Pin,0);
    HAL_GPIO_WritePin(BIN2_MB_GPIO_Port,BIN2_MB_Pin,0);
  }
}

void motor_2_setmode(uint8_t mode){
  if (mode==1){
    HAL_GPIO_WritePin(AIN1_MC_GPIO_Port,AIN1_MC_Pin,0);
    HAL_GPIO_WritePin(AIN2_MC_GPIO_Port,AIN2_MC_Pin,1);
  }
  else if (mode==2)
  {
    HAL_GPIO_WritePin(AIN1_MC_GPIO_Port,AIN1_MC_Pin,1);
    HAL_GPIO_WritePin(AIN2_MC_GPIO_Port,AIN2_MC_Pin,0);
  }
  else{
    HAL_GPIO_WritePin(AIN1_MC_GPIO_Port,AIN1_MC_Pin,0);
    HAL_GPIO_WritePin(AIN2_MC_GPIO_Port,AIN2_MC_Pin,0);
  }
}

void motor_3_setmode(uint8_t mode){
  if (mode==1){
    HAL_GPIO_WritePin(AIN1_MA_GPIO_Port,AIN1_MA_Pin,0);
    HAL_GPIO_WritePin(AIN2_MA_GPIO_Port,AIN2_MA_Pin,1);
  }
  else if (mode==2)
  {
    HAL_GPIO_WritePin(AIN1_MA_GPIO_Port,AIN1_MA_Pin,1);
    HAL_GPIO_WritePin(AIN2_MA_GPIO_Port,AIN2_MA_Pin,0);
  }
  else{
    HAL_GPIO_WritePin(AIN1_MA_GPIO_Port,AIN1_MA_Pin,0);
    HAL_GPIO_WritePin(AIN2_MA_GPIO_Port,AIN2_MA_Pin,0);
  }
}



void motor_1_setduty(uint8_t dutycycle){
    int pulse=0;
    if (dutycycle>100){
        dutycycle=100;
    }
    pulse=(dutycycle*3000)/100;
    __HAL_TIM_SET_COMPARE(&htim8,TIM_CHANNEL_1,pulse);
}

void motor_2_setduty(uint8_t dutycycle){
    int pulse=0;
    if (dutycycle>100){
        dutycycle=100;
    }
    pulse=(dutycycle*3000)/100;
    __HAL_TIM_SET_COMPARE(&htim8,TIM_CHANNEL_2,pulse);
}

void motor_3_setduty(uint8_t dutycycle){
    int pulse=0;
    if (dutycycle>100){
        dutycycle=100;
    }
    pulse=(dutycycle*3000)/100;
    __HAL_TIM_SET_COMPARE(&htim8,TIM_CHANNEL_3,pulse);
}

void motor_4_setduty(uint8_t dutycycle){
    int pulse=0;
    if (dutycycle>100){
        dutycycle=100;
    }
    pulse=(dutycycle*3000)/100;
    __HAL_TIM_SET_COMPARE(&htim8,TIM_CHANNEL_4,pulse);
}
