/*
 * hbridge.h
 *
 *  Created on: Apr 3, 2021
 *      Author: arnoutzwartbol
 */

#ifndef SRC_HBRIDGE_H_
#define SRC_HBRIDGE_H_

void motor_1_init(void);
void motor_2_init(void);
void motor_3_init(void);
void motor_4_init(void);

void motor_1_setduty(uint8_t dutycycle);
void motor_2_setduty(uint8_t dutycycle);
void motor_3_setduty(uint8_t dutycycle);
void motor_4_setduty(uint8_t dutycycle);

void motor_1_setmode(uint8_t mode);
void motor_2_setmode(uint8_t mode);
void motor_3_setmode(uint8_t mode);
void motor_4_setmode(uint8_t mode);

#endif /* SRC_HBRIDGE_H_ */
